import 'package:animal_planet_ui/utils/strings.dart';
import 'package:animal_planet_ui/utils/text_styles.dart';
import 'package:flutter/material.dart';

class custom_appbar extends StatefulWidget {
  @override
  _custom_appbarState createState() => _custom_appbarState();
}

class _custom_appbarState extends State<custom_appbar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 29, right: 12, left: 12),
      child: Row(
        children: [
          RichText(
            text: TextSpan(children: [
              TextSpan(
                text: Strings.APP_NAME,
                style: TextStyles.appNameTextStyle,
              ),
              TextSpan(text: "\n"),
              TextSpan(
                text: Strings.TAG_LINE,
                style: TextStyles.tagLineTextStyle,
              ),
            ]),
          ),
          Spacer(),
          Icon(
            Icons.menu,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
